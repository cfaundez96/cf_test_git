from pickle import TRUE
import sys

# +++your code here+++
# Define print_words(filename) and print_top(filename) functions.
# You could write a helper utility function that reads a file
# and builds and returns a word/count dict for it.
# Then print_words() and print_top() can just call the utility function.

###

# This basic command line argument parsing code is provided and
# calls the print_words() and print_top() functions which you must define.

def read_words(filename):
  hash = {}
  file = open(filename, 'r')
  for linea in file:
    palabras = linea.split()
    for p in palabras:
      p = p.lower()
      if not p.isalpha():
        for letra in p:
          if not letra.isalpha():
            p.replace(letra, '')
      if p in hash:
        hash[p] += 1
      else:
        hash[p] = 1
  return hash

def print_words(filename):
  hash = read_words(filename)
  print(hash)

def print_top(filename):
  hash = read_words(filename)
  hash = sorted(hash.items(), key=lambda x: x[1], reverse=True)
  if len(hash) > 20:
    hash = hash[:20]
  print(hash)

def main():
  if len(sys.argv) != 3:
    print ('usage: ./wordcount.py {--count | --topcount} file')
    sys.exit(1)

  option = sys.argv[1]
  filename = sys.argv[2]
  if option == '--count':
    print_words(filename)
  elif option == '--topcount':
    print_top(filename)
  else:
    print ('unknown option: ' + option)
    sys.exit(1)

if __name__ == '__main__':
  main()
