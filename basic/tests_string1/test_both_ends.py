#!/usr/bin/python -tt
from curses.ascii import isdigit

def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print ("%s got: %s expected: %s" % (prefix, repr(got), repr(expected)))

def both_ends(s):
  if len(s) < 2:
      return ''
  return s[:2] + s[-2:]


def main():
  print
  print ('both_ends')
  test(both_ends('spring'), 'spng')
  test(both_ends('Hello'), 'Helo')
  test(both_ends('a'), '')
  test(both_ends('xyz'), 'xyyz')


if __name__ == '__main__':
  main()