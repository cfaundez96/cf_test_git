#!/usr/bin/python -tt
from curses.ascii import isalpha, isdigit

from pytz import country_timezones

def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print ("%s got: %s expected: %s" % (prefix, repr(got), repr(expected)))


def donuts(count):
  if isalpha(count):
    return 'No number'
  if count >= 10:
    msg = 'many'
  else:
    msg = str(count)
  return 'Number of donuts: ' + msg

def main():
  print ('donuts')
  # Each line calls donuts, compares its result to the expected for that call.
  test(donuts(4), 'Number of donuts: 4')
  test(donuts(9), 'Number of donuts: 9')
  test(donuts(10), 'Number of donuts: many')
  test(donuts(99), 'Number of donuts: many')
  test(donuts('Hi'), 'No number')


if __name__ == '__main__':
  main()