from curses.ascii import isdigit

def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print ("%s got: %s expected: %s" % (prefix, repr(got), repr(expected)))

def fix_start(s):
  letra = s[0]
  return letra + s[1:].replace(letra, '*')

def main():
 print
 print ('fix_start')
 test(fix_start('babble'), 'ba**le')
 test(fix_start('aardvark'), 'a*rdv*rk')
 test(fix_start('google'), 'goo*le')
 test(fix_start('donut'), 'donut')

if __name__ == '__main__':
  main()