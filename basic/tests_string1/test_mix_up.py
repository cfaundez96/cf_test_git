def mix_up(a, b):
  x = b[:2]
  y = a[:2]
  _str1 = x + a[2:]
  _str2 = y + b[2:]
  return _str1 + ' ' + _str2

def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print ("%s got: %s expected: %s" % (prefix, repr(got), repr(expected)))

def main():
  print
  print ('mix_up')
  test(mix_up('mix', 'pod'), 'pox mid')
  test(mix_up('dog', 'dinner'), 'dig donner')
  test(mix_up('gnash', 'sport'), 'spash gnort')
  test(mix_up('pezzy', 'firm'), 'fizzy perm')


# Standard boilerplate to call the main() function.
if __name__ == '__main__':
  main()