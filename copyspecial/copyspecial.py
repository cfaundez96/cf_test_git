#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands

"""Copy Special exercise
"""

def get_special_paths(dir):
  ls = os.listdir(dir)
  for file in ls:
    m = re.search(r'.*__\w+__.*',file)
    if m:
      print(os.path.abspath(m.group()))

def copy_to(paths, dir):
  ls = os.listdir(dir)
  for path in paths:
    if not os.path.exists(path):
      os.mkdir(path)
    for file in ls:
      if not os.path.isdir(file):
        shutil.copy(file, path)
  
def zip_to(paths, zippath):
  files = ''
  for path in paths:
    ls = os.listdir(path)
    for file in ls:
      if os.path.isfile(file):
        files += os.path.abspath(file) + ' '
  cmd = 'zip -j ' + zippath + ' ' + files
  (status, output) = commands.getstatusoutput(cmd)

# +++your code here+++
# Write functions and modify main() to call them


def main():
  #get_special_paths(sys.argv[1])
  #copy_to(sys.argv[1:-1], sys.argv[-1])
  zip_to(['.'], 'test.zip')

"""def main():
  # This basic command line argument parsing code is provided.
  # Add code to call your functions below.

  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
  args = sys.argv[1:]
  if not args:
    print ("usage: [--todir dir][--tozip zipfile] dir [dir ...]")
    sys.exit(1)

  # todir and tozip are either set from command line
  # or left as the empty string.
  # The args array is left just containing the dirs.
  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    del args[0:2]

  tozip = ''
  if args[0] == '--tozip':
    tozip = args[1]
    del args[0:2]

  if len(args) == 0:
    print ("error: must specify one or more dirs")
    sys.exit(1)

  # +++your code here+++
  # Call your functions
"""
if __name__ == "__main__":
  main()
